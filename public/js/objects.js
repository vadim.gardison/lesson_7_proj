/*

  Objects

*/
  // 
  // var myObj = {
  //   logger: function(){
  //     console.log('logger', this);
  //   }
  // };
  //
  // var myObj2 = new Object();
      // myObj2.logger = function(){
      //   console.log('logger', this);
      // };
      //
      // myObj.logger();
      // myObj2.logger();
      // myObj2.myProp = "asd";
      // myObj2.logger();
      //
      // console.log( myObj2 );
      //
      // var settings = {
      //   colorsCount: 20,
      //   title: "MyObj",
      //   targetId: "wrap"
      // };

      // - - - - - -

      // function generateMyObject( settings ){
      //   this.colorsCount = settings.colorsCount;
      //   this.title = settings.title;
      //   this.targetId = settings.targetId;
      //
      //   function privateMethod(){
      //     this.name = "rrr";
      //   }
      //
      //   this.publicMethod = function(){
      //     console.log('v', this.name);
      //     privateMethod.apply(this);
      //     console.log('v', this.name);
      //   };
      //
      //   return this;
      // }
      //
      // var x = new generateMyObject(settings);
      //     x.publicMethod();


    /*

      Задание:

      Написать функцию генератор, которая будет иметь приватные и публичные свойства.
      Публичные методы должны вызывать приватные.

      Рассмотрим на примере планеты:

        - На вход принимаются параметр Имя планеты.

        Создается новый обьект, который имеет публичные методы и свойства:
        - name (передается через конструктор)
        - population ( randomPopulation());
        - rotatePlanet(){
          let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
          if ( (randomNumber % 2) == 0) {
            growPopulation();
          } else {
            Cataclysm();
          }
        }

        Приватные методы
        randomPopulation -> Возвращает случайное целое число от 1.000 до 10.000
        growPopulation() {
          функция которая берет приватное свойство populationMultiplyRate
          которое равняется случайному числу от 1 до 10 и умножает его на 100.
          Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
          что за один цикл прибавилось столько населения на планете .
        }
        Cataclysm(){
          Рандомим число от 1 до 10 и умножаем его на 10000;
          То число которое получили, отнимаем от популяции.
          В консоль выводим сообщение - от катаклизма погибло Х человек на планете.
        }


    */

    function Planet(name) {
      this.name = name;
      var populationMultiplyRate = Math.floor(Math.random() * (10 - 1 + 1)) + 1;
      console.log('rate',populationMultiplyRate);
      function randomPopulation() {
        return Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
      }
      function growPopulation() {

        console.log(populationMultiplyRate);
      }

      this.population = randomPopulation();
      this.rotatePlanet = function() {
        var randomNumber = Math.floor(Math.random() * (10 - 1 + 1)) + 1;
        // console.log(randomNumber);
        if ((randomNumber % 2) > 0) {
          growPopulation.apply(this);
        }
        // console.log('ok');
      }
      return this
    }

    var Mars = new Planet('Mars');
    console.log(Mars);
    console.log(Mars.rotatePlanet());
